#!/bin/bash
#
# CGI script to display DUC graphs of disk usage
# https://github.com/zevv/duc
#
# Stuart Anderson (Feb 12, 2015)
# Updated May 10, 2015 to support duc-1.1
# Updaetd March 5, 2016 to support duc-1.4.0 and click-to-sort tables
# Updated July 2019 to duc-1.4.4 and --list table side-by-side to graphs and remove individual user index files
#

AWK=/usr/bin/awk
CAT=/usr/bin/cat
DUC=/usr/bin/duc
SED=/usr/bin/sed
STAT=/usr/bin/stat
WEBROOT=/var/www/html/diskusage/
DUCDBDIR="$WEBROOT/ducdb"		# Location of duc index database files
LASTSCAN="$WEBROOT/lastscan"		# Timestamp file created at start of scan
CGI="$WEBROOT/index.cgi"		# Path to this CGI script
CGICACHE=/tmp/diskusage.html		# Location to cache html file of filesystem and user tables
CLUSTER=$($CAT /etc/ldasname)
case ${CLUSTER} in
    ldas-cit)
        SITENAME=CIT
        ;;
    ldas-la)
        SITENAME=LLO
        ;;
    ldas-wa)
        SITENAME=LHO
        ;;
    *)
        SITENAME=UNKNOWN
        ;;
esac

shopt -s nullglob

#
# Initialize html page
# $1: path to a duc database file
#
initializepage() {
    if [ $# -ne 1 ]; then
        echo "Internal error: initializepage() called with $# rather than 1 arguments"
        exit 1
    fi

    #
    # Print html header if not already generated, i.e., everything up to and including the first </head>,
    # with load command for sorttable.js and some table header stylesheet,
    # http://www.kryogenix.org/code/browser/sorttable/#symbolsbeforesorting
    # Also set target=_blank to have links open in new tab
    # And print a timestamp summary line rather than printing one for every row
    #
    local style='table.sortable thead { background-color:#eee; color:#666666; font-weight: bold; cursor: default } table.sortable tbody { counter-reset: sortabletablescope; } table.sortable thead tr::before { content: ""; display: table-cell; } table.sortable tbody tr::before { content: counter(sortabletablescope); counter-increment: sortabletablescope; display: table-cell; } table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { content: " \\25B4\\25BE" }table.sortable tbody tr:nth-child(2n) td { background: #eef5dc; padding: 3px } table.sortable tbody tr:nth-child(2n+1) td { background: #dcf5e1; padding: 3px}'
    $DUC cgi -d $1 | $SED 's:<head>:<head><script src="./sorttable.js"></script>:' | $SED "s%</style>%$style</style>%" | $SED '/<\/head>/q'
    local scanstart=$($STAT -c %Y "$LASTSCAN")
    local lastduc=$($STAT -c %Y "$1")
    cat <<-EOF
	<base target="_blank">
	<center>
	<h1>${CLUSTER^^} Cluster Storage</h1>
        <figure><a href="https://xkcd.com/2582"><img src="data_trap.png"></a><figcaption>(from <a href="https://xkcd.com/2582">xkcd.com</a>)</figcaption></figu
re>
	EOF
    if [ -f "$LASTSCAN" ]; then
	cat <<-EOF
	    <h2>Current update started $(date --date="@$scanstart")</h2>
	EOF
    fi
    cat <<-EOF
	<h2>Last individual filesystem scan completed $(date --date="@$lastduc")</h2>
	<p>
	</center>
	EOF
}


#
# Initialize table
#
initializetable() {
    #
    # Associative arrays for User table
    #
    declare -gA TableActiveFilesystems
    declare -gA ActiveFilesystem
    declare -gA ActivePath
    declare -gA ActiveDatabase
    declare -gA ActiveSize
    declare -gA ActiveApparentSize
    declare -gA ActiveCount
    declare -gA TableColdFilesystems
    declare -gA ColdFilesystem
    declare -gA ColdPath
    declare -gA ColdDatabase
    declare -gA ColdSize
    declare -gA ColdApparentSize
    declare -gA ColdCount
    declare -gA User

    #
    # Clear table column counts
    #
    tablesize=0
    tableapparentsize=0
    tablefiles=0
    tabledirectories=0
    tablerows=0
    tabledownload=0
    tableepochstart=0
    tableepochend=0
    tableindex="current"
    tableindexduration=0

    #
    # Column specific style settings
    #
    ActiveStyle="background-color: #e0d496; padding: 5px; text-align:center"
    ColdStyle="background-color: #dbe096; padding: 5px; text-align:center"
    SubTitleStyle="font-size: 1.25em"
}


#
# Geneate an html table row for a filesystem
# $1: path to duc database file
#
printfilesystemrow() {
    #
    # Print table row entry with "duc info" data for specified filesystem database.
    #

    #
    # Get duc index start time from ducdb and end time from stat
    # Truncate dispaly times after minutes, but keep seconds for internal calculations
    # Check whether ducdb is more recent than the latest cron initiated scan start time
    #
    local EpochCronRunning=$($STAT -c %Y "$LASTSCAN")
    local EpochCronDone=$($STAT -c %Y "${LASTSCAN}.done")
    local timestamp=$($DUC info -d $1 | $AWK 'END{print $1, $2}')
    local EpochStart=$(date --date="$timestamp" +"%s")
    local IndexStart=$(date --date="@$EpochStart" +"%b %d %H:%M")
    local EpochCompleted=$($STAT -c %Y $1)
    local IndexCompleted=$(date --date="@$EpochCompleted" +"%b %d %H:%M")
    local IndexDuration=$((EpochCompleted - EpochStart))
    local Status="current"

    if [ $EpochCompleted -lt $EpochCronDone ]; then
	Status="old"
    elif [ $EpochStart -lt $EpochCronRunning ]; then
	Status="pending"
	tablestatus="pending"
    fi

    local files directories size path
    read files directories size path <<<$($DUC info -b -d $1 | $AWK 'END{print $3, $4, $5, $6}')
    local apparentsize=$($DUC info -a -b -d $1 | $AWK 'END{print $5}')
    local apparentsizehuman=$($DUC info -a -d $1 | $AWK 'END{print $5}')
    local download=$($STAT -c %s $1)

    ((tablesize+=size))
    ((tableapparentsize+=apparentsize))
    ((tablefiles+=files))
    ((tabledirectories+=directories))
    ((tabledownload+=download))
    ((tablerows++))
    if [ $tableepochstart -eq 0 ] || [ $EpochStart -lt $tableepochstart ]; then
	tableepochstart=$EpochStart
	tableindexstart=$IndexStart
    fi
    if [ $tableepochend -eq 0 ] || [ $EpochCompleted -gt $tableepochend ]; then
	tableepochend=$EpochCompleted
	tableindexend=$IndexCompleted
    fi
    [ $EpochCompleted -gt $tableepochend ] && tableepochend=$EpochCompleted
    tableindexduration=$((tableepochend - tableepochstart))

    basename=${1##*/}
    local filesystem=${basename%.duc}

    url_size="/diskusage/index.cgi?path=$path&database=$1"
    url_apparent="/diskusage/apparent.cgi?path=$path&database=$1"
    url_count="/diskusage/count.cgi?path=$path&database=$1"

    cat <<-EOF
	<tr>
	<td><a href="$url_size">$filesystem</a></td>
	EOF
    PrintHumanNumber "$size" "byte" "$url_size"
    PrintHumanNumber "$apparentsize" "byte" "$url_apparent"
    PrintHumanNumber "$files" "int" "$url_count"
    PrintHumanNumber "$directories" "int" "$url_count"

    cat <<-EOF
	<td sorttable_customkey=$download align="right"><a href="https:${1#$WEBROOT}">$(ConvertHumanNumber $download "byte")</a></td>
	<td style="text-align:center">$Status</td>
	<td sorttable_customkey=$EpochStart>$IndexStart</td>
	<td sorttable_customkey=$EpochCompleted>$IndexCompleted</td>
	EOF
    PrintHumanTime $IndexDuration
    echo "</tr>"

    ((tablerows++))
}


#
# Geneate an html table row for a user
# $1: user
#
printuserrow() {
    #
    # Print table row entry for indicated user showing both active and cold storage
    #
    local filesystem=${ActiveFilesystem[$1]}
    local path=${ActivePath[$1]}
    local database=${ActiveDatabase[$1]}
    local size=${ActiveSize[$1]}
    local apparentsize=${ActiveApparentSize[$1]}
    local files=${ActiveCount[$1]}
    local coldfilesystem=${ColdFilesystem[$1]}
    local coldpath=${ColdPath[$1]}
    local colddatabase=${ColdDatabase[$1]}
    local coldsize=${ColdSize[$1]}
    local coldapparentsize=${ColdApparentSize[$1]}
    local coldfiles=${ColdCount[$1]}

    ((tablesize+=size))
    ((tableapparentsize+=apparentsize))
    ((tablefiles+=files))
    ((tablecoldsize+=coldsize))
    ((tablecoldapparentsize+=coldapparentsize))
    ((tablecoldfiles+=coldfiles))
    ((tablerows++))

    [ $filesystem ] && TableActiveFilesystems["$filesystem"]=
    [ $coldfilesystem ] && TableColdFilesystems["$coldfilesystem"]=

    url_fs="/diskusage/index.cgi?path=$path&database=$database"
    url_size="/diskusage/index.cgi?path=$path&database=$database"
    url_apparent="/diskusage/apparent.cgi?path=$path&database=$database"
    url_count="/diskusage/count.cgi?path=$path&database=$database"

    url_cold_fs="/diskusage/index.cgi?path=$coldpath&database=$colddatabase"
    url_cold_size="/diskusage/index.cgi?path=$coldpath&database=$colddatabase"
    url_cold_apparent="/diskusage/apparent.cgi?path=$coldpath&database=$colddatabase"
    url_cold_count="/diskusage/count.cgi?path=$coldpath&database=$colddatabase"

    #
    # Add id attribute for the first name that starts with each letter
    #
    local first=${1::1}		# Get the first letter of a user name
    local letter=${first^}	# Capatalize the first letter
    local id=""
    local top=""
    if [[ "$letter" == [A-Z] && -z ${User["$letter"]} ]]; then
	#
	# First user for this letter
	#
	User["$letter"]=$1
	id="id=\"$letter\" "
	top='<span style="float:right;"><a href="#user" target="_parent">Top</a></span>'
    fi

    echo "<tr>"
    if [ "$filesystem" ]; then
	echo " <td><a ${id}href=\"$url_fs\">$1</a>$top</td>"
    elif [ "$coldfilesystem" ]; then
	echo "<td><a ${id}href=\"$url_cold_fs\">$1</a>$top</td>"
    else
	echo "<td>$1 NO STORAGE?</td>"
    fi
    echo "<td><a href=\"$url_fs\">$filesystem</a></td>"
    PrintHumanNumber "$size" "byte" "$url_size"
    PrintHumanNumber "$apparentsize" "byte" "$url_apparent"
    PrintHumanNumber "$files" "int" "$url_count"

    echo "<td><a href=\"$url_cold_fs\">$coldfilesystem</a></td>"
    PrintHumanNumber "$coldsize" "byte" "$url_cold_size"
    PrintHumanNumber "$coldapparentsize" "byte" "$url_cold_apparent"
    PrintHumanNumber "$coldfiles" "int" "$url_cold_count"
    echo "</tr>"

    ((tablerows++))
}


#
# Print Number in with human readable units
# $1 value
# $2 units
# $3 URL
#
PrintHumanNumber(){
    if [ -z $1 ]; then
	echo "<td></td>"
    else
	echo -n "<td sorttable_customkey=$1 align=\"right\">"
	[ "$3" ] && echo -n "<a href=\"$3\">"
	ConvertHumanNumber "$1" "$2"
	[ "$3" ] && echo -n "</a>"
	echo "</td>"
    fi
}


#
# $1 value
# $2 units
#
ConvertHumanNumber(){
	if [ "$2" == "byte" ]; then
	    #
	    # Print byte in with human readable units (B, KB, MB, GB, TB, PB)
	    #
	    echo -n $(echo $1 | $AWK '{ sum=$1 ; hum[1024**5]="PB";hum[1024**4]="TB";hum[1024**3]="GB";hum[1024**2]="MB";hum[1024]="KB";hum[1]="B"; for (x=1024**5; x>=1024; x/=1024){ if (sum>=x) { printf "%.1f %s\n",sum/x,hum[x];exit } } print sum,hum[1]}')
	else
	    #
	    # Print number with units (K, M, G, T, P)
	    #
	    echo $(echo $1 | $AWK '{ sum=$1 ; hum[1000**5]="P";hum[1000**4]="T";hum[1000**3]="G";hum[1000**2]="M";hum[1000]="K"; for (x=1000**5; x>=1000; x/=1000){ if (sum>=x) { printf "%.1f %s\n",sum/x,hum[x];exit } } print sum}')
	fi
}


#
# Display seconds as day(s) hour(s) minute(s) [second(s)]
# $1 value
# $2 display <$3> if present
#
PrintHumanTime()
{
    if [ -z $1 ]; then
	echo "<td></td>"
    else
	local t=$1
	local d=$((t/60/60/24))
	local h=$((t/60/60%24))
	local m=$((t/60%60))
	local s=$((t%60))

	#
	# Add blank "0" and blank "s" for alignment in table if not bold footer
	#
	if [ $# -eq 2 ]; then
	    local blankdigit=""
	    local blankletter=""
	else
	    local blankdigit="<span style=\"opacity:0;\">0</span>"
	    local blankletter="<span style=\"opacity:0;\">s</span>"
	fi

	echo -n "<td sorttable_customkey=$1 style=\"text-align:right\">"
	[ $# -eq 2 ] && echo -n "<$2>"

	if (( $d > 0 )); then
	    if (( $d == 1 )); then
		echo "$blankdigit$d day$blankletter"
	    elif (( $d < 10)); then
		echo "$blankdigit$d days"
	    else
		echo "$d days"
	    fi
	fi

	if (( $h > 0 )); then
	    if (( $h == 1 )); then
		echo "$blankdigit$h hour$blankletter"
	    elif (( $h < 10)); then
		echo "$blankdigit$h hours"
	    else
		echo "$h hours"
	    fi
	fi

	if (( $m > 0 )); then
	    if (( $m == 1 )); then
		echo "$blankdigit$m minute$blankletter"
	    elif (( $m < 10)); then
		echo "$blankdigit$m minutes"
	    else
		echo "$m minutes"
	    fi
	fi

	#
	# Optionally print seconds only if nothing else displayed
	#
	if (( $d == 0 && $h == 0 && $m == 0 )); then
            (( $s == 1 )) && echo -n "$s second" || echo -n "$s seconds"
	fi
	[ $# -eq 2 ] && echo -n "</$2>"
	echo "</td>"
    fi
}


#
# Table html header
#
tableheader() {
    initializetable
    if [[ "$1" =~ ^Filesystem ]]; then
	#
	# Filesystem table
	#
	cat <<-EOF
	<h1><a href="#filesystem" target="_parent">Filesystem Table</a></h1>
		<big>
		<ul>
		<li> <strong>Click on table entries</strong> to view a clickable map and table of data usage or to download a duc database file for offline viewing. </li>
		<li> <strong>Click on column headings</strong> to sort table. </li>
			<ul>
			<li> <strong>Filesystem:</strong> filesystem or ZFS pool hosting data. </li>
			<li> <strong>Size:</strong> the amount of disk space used as reported by the /bin/du command. Note, this can vary from /bin/ls due to compression or files being offline on tape. </li>
			<li> <strong>Apparent:</strong> sum of file sizes as reported by the /bin/ls command. </li>
			<li> <strong>Files:</strong> total number of files. </li>
			<li> <strong>Directories:</strong> total number of directories. </li>
			<li> <strong>Database:</strong> size of duc database avallable to download for offline viewing. </li>
			<li> <strong>Status:</strong> duc scan status: </li>
				<ul>
				<li> <strong>Current</strong> scan up to date. </li>
				<li> <strong>Pending</strong> scan actively running. </li>
				<li> <strong>Old</strong> no scan running or completed since the last table update started. </li>
				</ul>
			<li> <strong>Start:</strong> time last duc index started in $(date +"%Z %z") time zone. </li>
			<li> <strong>Completed:</strong> time last completed duc index finished in $(date +"%Z %z") time zone. </li>
			<li> <strong>Duration:</strong> time duc index took to run. </li>
			</ul>
		</ul>
		</big>
		EOF
    else
	cat <<-EOF
	<h1><a href="#user" target="_parent">User Table</a></h1>
		<big>
		<ul>
		<li> <strong>Click on table entries</strong> to view a clickable map and table of data usage or to download a duc database file for offline viewing. </li>
		<li> <strong>Click on column headings</strong> to sort table. </li>
			<ul>
			<li> <strong>User:</strong> cluster account name. </li>
			<li> <strong>Active:</strong> online storage used for frequently accessed data. </li>
			<li> <strong>Cold:</strong> hierarchical storage used for infrequently accessed data where only recently accessed data are online and most data are on tape that may take longer to retreive.</li>
				<ul>
				<li>Note, user accounts are migrated to Cold storage when a user leaves the collaboration, but infrequently used directories in otherwise active accounts may also be migrated to Cold storage to free up online space. </li>
				</ul>
			</ul>
		</ul>
		</big>
		EOF
    fi
    cat <<-EOF
	<p>
	<center>
	<body>
	<div id=main>
	<div id=index>
	<table class=sortable><thead>
	EOF

    local TitleStyle="font-size: 1.75em; padding: 10px; background-color: #b5f2f7"
    if [[ "$1" =~ ^Filesystem ]]; then
	#
	# Filesystem table
	#
	local PaddingStyle="padding: 5px"
	cat <<-EOF
		<div id="filesystem">
		<tr>
		<th title="Click to sort" style="$TitleStyle">$1</th>
		<th title="Click to sort" style="$PaddingStyle">Size</th>
		<th title="Click to sort" style="$PaddingStyle">Apparent</th>
		<th title="Click to sort" style="$PaddingStyle">Files</th>
		<th title="Click to sort" style="$PaddingStyle">Directories</th>
		<th title="Click to sort" style="$PaddingStyle">Database</th>
		<th title="Click to sort" style="$PaddingStyle">Status</th>
		<th title="Click to sort" style="$PaddingStyle">Start</th>
		<th title="Click to sort" style="$PaddingStyle">Completed</th>
		<th title="Click to sort" style="$PaddingStyle">Duration</th>
		</tr>
		EOF
    else
	#
	# User table
	#
	cat <<-EOF
		<div id="user"><big>
		<a id="User"><strong>User:</strong></a>
		EOF
	for letter in {A..Z}; do
	    cat <<-EOF
		<a href="#$letter" target="_parent">$letter</a>
		EOF
	done
	cat <<-EOF
		</big>
		<p>
		<tr>
		<th title="Click to sort" style="$TitleStyle">$1</th>
		<th title="Click to sort" style="$SubTitleStyle; $ActiveStyle">Active</th>
		<th title="Click to sort" style="$ActiveStyle">Size</th>
		<th title="Click to sort" style="$ActiveStyle">Apparent</th>
		<th title="Click to sort" style="$ActiveStyle">Files</th>
		<th title="Click to sort" style="$SubTitleStyle; $ColdStyle">Cold</th>
		<th title="Click to sort" style="$ColdStyle">Size</th>
		<th title="Click to sort" style="$ColdStyle">Apparent</th>
		<th title="Click to sort" style="$ColdStyle">Files</th>
		</tr>
		EOF
    fi

    cat <<-EOF
	</thead>
	<tbody>
	EOF
}


#
# Table footer with summary numbers
#
tablefooter() {
    [ "$tablerows" -gt 0 ] || return;
    local Total="<td style=\"text-align:left; font-size: 1.5em\">Total</td>"
    cat <<-EOF
	</tbody>
	<tfoot style="text-align:right; font-size: 1.1em; font-weight: bold">
	<tr>
	EOF

    if [[ "$1" =~ ^Filesystem ]]; then
	#
	# Filesystem table
	#
	cat <<-EOF
		<td></td>
		$Total
		EOF
	PrintHumanNumber "$tablesize" "byte" ""
	PrintHumanNumber "$tableapparentsize" "byte" ""
	PrintHumanNumber "$tablefiles" "int" ""
	PrintHumanNumber "$tabledirectories" "int" ""
	PrintHumanNumber "$tabledownload" "byte" ""
	echo "<td style="text-align:center">$tableindex</td>"
	echo "<td>$tableindexstart</td>"
	echo "<td>$tableindexend</td>"
	PrintHumanTime $tableindexduration
    else
	#
	# User table (add column headers in footer)
	#
	cat <<-EOF
		<td colspan=2></td>
		<td style="$SubTitleStyle; $ActiveStyle">Active</td>
		<td style="$ActiveStyle">Size</td>
		<td style="$ActiveStyle">Apparent</td>
		<td style="$ActiveStyle">Files</td>
		<td style="$SubTitleStyle; $ColdStyle">Cold</td>
		<td style="$ColdStyle">Size</td>
		<td style="$ColdStyle">Apparent</td>
		<td style="$ColdStyle">Files</td>
		</tr>
		<tr>
		<td></td>
		$Total
		EOF
	PrintHumanNumber "${#TableActiveFilesystems[@]}" "int" ""
	PrintHumanNumber "$tablesize" "byte" ""
	PrintHumanNumber "$tableapparentsize" "byte" ""
	PrintHumanNumber "$tablefiles" "int" ""
	PrintHumanNumber "${#TableColdFilesystems[@]}" "int" ""
	PrintHumanNumber "$tablecoldsize" "byte" ""
	PrintHumanNumber "$tablecoldapparentsize" "byte" ""
	PrintHumanNumber "$tablecoldfiles" "int" ""
    fi

    cat <<-EOF
	</tr>
	</foot>
	</table>
	</div>
	</center>
	EOF
}


#
# Generate list of all toplevel directories in a list of .duc files
# arg1 = glob for index files
# arg2 = optional sub-directory to list
#
toplevel () {
    for ducdb in $1; do
        top=$($DUC info -d $ducdb | $AWK 'END{print $NF}')
        $DUC ls -d $ducdb $top/$2 | $AWK -v ducdb=$ducdb -v top=$top '$2 !~ /^\./ && $2 != "lost+found" {print ducdb, top, $2}'
    done
}


#
# Generate end of page tags
#
pagefooter() {
    echo "Data generated and displayed by <a href=\"https://github.com/zevv/duc\">$($DUC --version)</a>"
    echo "</html>"
}


if [ "$QUERY_STRING" ]; then
    #
    # Display clickable CGI canvas.
    #
    # Requires patch to src/duc/cmd-cgi.c to pass &database=$ducdb whenver passing &path=$path
    # to keep track of which datase file is being displayed.
    #
    # Also consider patch to keep track of mode (size, apparaent, count) via query_string
    # rather than symbolic link names to index.cgi
    #
    # Parse the CGI arguments.
    #
    saveIFS=$IFS
    IFS='=&'
    QUERY_STRING=${QUERY_STRING//\%2e/.}	# Replace HTML escaped sequence with literal "."
    parm=($QUERY_STRING)
    IFS=$saveIFS

    declare -A array
    for ((i=0; i<${#parm[@]}; i+=2))
    do
        array[${parm[i]}]=${parm[i+1]}
    done

    ducdb=${array[database]}			# Available from patching src/duc/cmd-cgi.c
    path=${array[path]}

    #
    # Center table and add "Close this window" button after table
    # If called as apparent.cgi then pass the -a argument to duc to show the apparent file size
    # If called as count.cgi than pass the --count argument to duc to show file count
    # By default (index.cgi) show the actual disk usage
    # Add buttons to switch between these 3 modes and add apparent size number to table
    #
    TMP=${REQUEST_URI/*\?/index.cgi?}
    DISKUSAGEURI=${TMP//&/\\&}
    diskusage="<input type=\"button\" onclick=\"location.href=\'$DISKUSAGEURI\';\" value=\"disk usage\" />"

    TMP=${REQUEST_URI/*\?/apparent.cgi?}
    FILESIZEURI=${TMP//&/\\&}
    filesize="<input type=\"button\" onclick=\"location.href=\'$FILESIZEURI\';\" value=\"apparent size\" />"

    TMP=${REQUEST_URI/*\?/count.cgi?}
    FILECOUNTURI=${TMP//&/\\&}
    filecount="<input type=\"button\" onclick=\"location.href=\'$FILECOUNTURI\';\" value=\"file count\" />"

    focus="style=\"font-size: 1.25em\""
    pathheader="<th>Path</th>"
    sizeheader="<th>Size</th>"
    apparentheader="<th>Apparent</th>"
    countheader="<th>Files</th>"
    if [[ "$SCRIPT_FILENAME" =~ "count.cgi" ]]; then
	ARG="--count"
	countheader="<th $focus>Files</th>"
	filecount="FILE COUNT"
	description="Map of File Count"
    elif [[ "$SCRIPT_FILENAME" =~ "apparent.cgi" ]]; then
	ARG="-a"
	apparentheader="<th $focus>Apparent</th>"
	filesize="APPARENT SIZE"
	description="Map of Apparent Disk Usage"
    else
	ARG=""
	sizeheader="<th $focus>Size</th>"
	diskusage="DISK USAGE"
	description="Map of Actual Disk Usagee"
    fi
    tableheader="<tr>$pathheader$sizeheader$apparentheader$countheader</tr>"
    download="<input type=\"button\" onclick=\"location.href=\'${ducdb#$WEBROOT}\';\" value=\"download duc database \($(/usr/bin/du -h $ducdb | $AWK '{print $1}')\)\" />"


    top=$($DUC info -d $ducdb | $AWK 'END{print $NF}')
    if [ "$path" = "$top" ]; then
	read files directories <<<$($DUC info -b -d $ducdb | $AWK 'END{print $3, $4}')
	rawcount=$((files+directories))
	read rawsize <<<$($DUC info -b -d $ducdb | $AWK 'END{print $5}')
	read rawapparent <<<$($DUC info --apparent -b -d $ducdb | $AWK 'END{print $5}')
    else
	parentpath="$path/.."
	basename=${path##*/}
	rawcount=$($DUC ls --count -b -d $ducdb $parentpath | $AWK -v name=$basename '$2 == name {print $1}')
	rawsize=$($DUC ls -b -d $ducdb $parentpath | $AWK -v name=$basename '$2 == name {print $1}')
	rawapparent=$($DUC ls --apparent -b -d $ducdb $parentpath | $AWK -v name=$basename '$2 == name {print $1}')
    fi
    count=$(ConvertHumanNumber $rawcount "int")
    size=$(ConvertHumanNumber $rawsize "byte")
    apparent=$(ConvertHumanNumber $rawapparent "byte")
    url_size="/diskusage/index.cgi?path=$path\&database=$ducdb"
    url_apparent="/diskusage/apparent.cgi?path=$path\&database=$ducdb"
    url_count="/diskusage/count.cgi?path=$path\&database=$ducdb"
    tablerow="<tr><td><a href=\"$url_size\">$path</a></td><td><a href=\"$url_size\">$size</a></td><td><a href=\"$url_apparent\">$apparent</a></td><td><a href=\"$url_count\">$count</a></td></tr>"

    style='table { border-collapse: collapse; } #t01 th { border: 1px solid #dddddd; padding: 8px; background: #eef5dc} #t01 td { border: 1px solid #dddddd; padding: 8px; background: #dcf5e1; text-align: center}\n'
    table="<h3><table id=\"t01\">$tableheader$tablerow</table></h3>"
    button="<h2>$diskusage $filesize $filecount</h2>"

    #
    # Replace default Path from "duc info" in "duc cgi" table with $fielsystem to avoid
    # ambiguity when multiple filesystems have the same top level directory, e.g.,
    # samhome1:/home1 and zfshome1:/home1
    #
    basename=${ducdb##*/}
    filesystem=${basename%.duc}
    topanchor=">$top<"

    title="<h1>$description</h1><h2>(click to zoom in on a directory)</h2>"
    #
    # sed "0,/foor/s@@bar@" replaces only the first occurance of foo with bar
    #
    $DUC cgi $ARG --tooltip --list -d $ducdb | $SED "0,/Path/s@@Filesystem@" | $SED "s%$topanchor%>$filesystem<%" | $SED "s%</style>%$style</style>%" | $SED "0,/<table>/s@@<center>$title$table\n$button\n<table>@" | $SED "0,/<div id=graph>/s@@<h2>$download</h2><p>\n<div id=graph>@"

else
    #
    # Display table of statistics with links to individual ducdb graphs.
    #
    lastducdb=$(find $DUCDBDIR -type f -name "*.duc" -printf "%T@ %p\n" | sort -n | $AWK 'END{print $NF}')
    echo "find $DUCDBDIR -type f -name \"*.duc\" -printf \"%T@ %p\\n\" | sort -n | $AWK 'END{print $NF}'" >> /tmp/debug.log

    if [[ "$(find $CGICACHE -newer $lastducdb -newer $DUCDBDIR/filesystem -newer $CGI 2> /dev/null)" ]] && [[ ! -f $LASTSCAN || "$(find $CGICACHE -newer $LASTSCAN 2> /dev/null)" ]]; then
	#
	# Use cached copy.
	#
	$CAT $CGICACHE
    else
	#	
	# Generate new tables and update cache.
	#
	CGICACHETMP=$(mktemp --tmpdir=$(dirname $CGICACHE))

	{
	#
	# Generate table of individual filesystems.
	#
	initializepage $lastducdb
	tableheader "Filesystem"
	for ducdb in $DUCDBDIR/filesystem/*.duc; do
	    printfilesystemrow $ducdb
	done
	tablefooter "Filesystem"

	#
	# Scan active filesystems for users
	#
	for ducdb in $DUCDBDIR/filesystem/*home*.duc; do
	    basename=${ducdb##*/}
	    filesystem=${basename%.duc}
	    top=$($DUC info -d $ducdb | $AWK 'END{print $NF}')
	    rootpath=$top
	    while read -r size user; do
                if [[ $user =~ ^\. ]] ; then
                    continue
                fi
		ActiveSize[$user]=$size
		ActivePath[$user]="$rootpath/$user"
		ActiveDatabase[$user]=$ducdb
		ActiveFilesystem[$user]=$filesystem
	    done < <($DUC ls -b -d $ducdb $rootpath)
	    while read -r apparent user; do
		ActiveApparentSize[$user]=$apparent
	    done < <($DUC ls -b --apparent -d $ducdb $rootpath)
	    while read -r count user; do
		ActiveCount[$user]=$count
	    done < <($DUC ls -b --count -d $ducdb $rootpath)
	done

	#
	# Scan cold storage filesystems for users
	#
	for ducdb in $DUCDBDIR/filesystem/oldacct?.duc $DUCDBDIR/filesystem/cold?.duc; do
	    basename=${ducdb##*/}
	    filesystem=${basename%.duc}
	    top=$($DUC info -d $ducdb | $AWK 'END{print $NF}')
	    rootpath="$top/${SITENAME}"
	    while read -r size user; do
		ColdSize[$user]=$size
		ColdPath[$user]="$rootpath/$user"
		ColdDatabase[$user]=$ducdb
		ColdFilesystem[$user]=$filesystem
	    done < <($DUC ls -b -d $ducdb $rootpath)
	    while read -r apparent user; do
		ColdApparentSize[$user]=$apparent
	    done < <($DUC ls -b --apparent -d $ducdb $rootpath)
	    while read -r count user; do
		ColdCount[$user]=$count
	    done < <($DUC ls -b --count -d $ducdb $rootpath)
	done

	tableheader "User"
	while read -r u; do
	    printuserrow $u $ducdb
	done < <(printf '%s\n' ${!ActiveSize[@]} ${!ColdSize[@]} | sort --ignore-case --unique)
	tablefooter "User"

	pagefooter
	} | tee $CGICACHETMP
	/bin/rm -f $CGICACHE
	/bin/mv $CGICACHETMP $CGICACHE
    fi
fi
