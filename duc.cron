#!/bin/ksh
#
# Script to index filesystems with the duc program and copy the
# resulting database files to a web server to be viewed at,
# http://ldas-gridmon.ligo.caltech.edu/diskusage
# 
# What to do with zfs-backup, which had filesystems unmounted to
# improve performance?
#


#
# Hardcoded paths for security.
#
HOSTNAME=$(hostname)
RM=/usr/bin/rm
MV=/usr/bin/mv
SSH=/usr/bin/ssh
SSHARG="-x -o ServerAliveInterval=60"
MKDIR=/usr/bin/mkdir
FIND=/usr/bin/find
TOUCH=/usr/bin/touch
PARALLEL=/usr/bin/parallel
CURL=/usr/bin/curl
SCP=/usr/bin/scp
SSHADD=/usr/bin/ssh-add
LOCK=/tmp/duc.lock
GATEWAY=root@ldas-cit.ligo.caltech.edu
WEBHOST=gridmon
WEBDIR=/var/www/html/diskusage
WEBURL="https://ldas-gridmon.ligo.caltech.edu/diskusage/"
WEBHOSTPUB=root@ldas-gridmon.ligo.caltech.edu
LASTSCAN=lastscan
SCAN=duc_scan.sh
SCANDIR=~/etc
PID=$$


#
# Associative array of arrays of hostnames with their filesystems and mount points
#
filesystem=(
#    ['grid']=(
#	['hdfs']=(
#	    ['type']='filesystem'
#	    ['mount']='/hdfs'
#	    ['sub']='.'
#	    ['tmp']='/local'
#	)
#    )
#    ['migrate']=(
#	['migrate']=(
#	    ['type']='filesystem'
#	    ['mount']='/migrate'
#	    ['sub']='.'
#	    ['tmp']='/dev/shm'
#	)
#    )
    ['vsmarchive']=(
	['archive']=(
	    ['type']='filesystem'
	    ['mount']='/archive/frames'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
	['losc']=(
	    ['type']='filesystem'
	    ['mount']='/archive/losc'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
	['backup']=(
	    ['type']='filesystem'
	    ['mount']='/backup'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
	['cold1']=(
	    ['type']='filesystem'
	    ['mount']='/cold1'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
	['cold2']=(
	    ['type']='filesystem'
	    ['mount']='/cold2'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
	['cold3']=(
	    ['type']='filesystem'
	    ['mount']='/cold3'
	    ['sub']='.'
	    ['tmp']='/dev/shm'
	)
    )
    ['zfsnfs2']=(
        ['ifocache']=(
            ['type']='filesystem'
            ['mount']='/ifocache'
            ['sub']='.'
	    ['tmp']='/tmp'
        )
    )
    ['zfsscratch']=(
        ['scratch']=(
            ['type']='filesystem'
            ['mount']='/scratch'
            ['sub']='.'
	    ['tmp']='/dev/shm'
        )
    )
)

for i in {1..4}; do
    typeset -A filesystem["zfshome$i"]=()
    typeset -A filesystem["zfshome$i"]["zfshome$i"]=()
    filesystem["zfshome$i"]["zfshome$i"]['type']='filesystem'
    filesystem["zfshome$i"]["zfshome$i"]['mount']="/home$i"
    filesystem["zfshome$i"]["zfshome$i"]['sub']="."
    filesystem["zfshome$i"]["zfshome$i"]['tmp']="/dev/shm"
done


#
# Allow this bash script to parse the csh formatted ssh-agent cache file
#
setenv() { export "$1=$2"; }


#
# Function to print log messages
#
printlog () {
    echo "$(date) $HOSTNAME $@"
}


printlog "Started pid $PID"

#
# Connect to the ssh agent.
#
source /home/ldasadm/.agent
$SSHADD -l > /dev/null 2>&1

if [ $? -ne 0 ]; then
    echo "Error: Unable to connect to ssh agent." > /dev/stderr
    exit 1
fi


#
# Check for lock file.
#
if [ -f $LOCK ]; then
    echo "Error: Lock file [$LOCK] already exists." > /dev/stderr
    exit 1
fi


#
# Create lockfile and run the actual job.
#
echo $PID > $LOCK


#
# Make destination directories on web server for each server and filesysetm type
# and touch timestamp file on web server to indicate a new scan has been started.
#
ARG="-p"
typeset -A globaltypelist=( [filesystem]=1 )	# Every host runs a filesystem duc scan
for host in "${!filesystem[@]}"
do
    for fs in "${!filesystem[$host][@]}"
    do
	type=${filesystem[$host][$fs][type]}
	((globaltypelist[$type]++))
    done
done
for type in "${!globaltypelist[@]}"
do
    ARG+=" $WEBDIR/ducdb/$type $WEBDIR/log/$type $WEBDIR/stat/$type"
done
$SSH $SSHARG $WEBHOSTPUB "$MKDIR $ARG && $TOUCH $WEBDIR/$LASTSCAN"


#
# Update scan script on gateway
#
$SCP -q $SCANDIR/$SCAN ${GATEWAY}:

#
# Loop over file servers and launch duc_scan.sh on each server after tirggering
# an iniital cgi cache update to show that a new scan has started.
#
printlog "File server scans started"
$CURL -s -o /dev/null $WEBURL

for host in "${!filesystem[@]}"
do
    COPY="$SCP -q $SCAN ${host}: && "
    for fs in "${!filesystem[$host][@]}"
    do
	ARG="$WEBHOST $WEBDIR $host"
	type=${filesystem[$host][$fs][type]}
	mount=${filesystem[$host][$fs][mount]}
	sub=${filesystem[$host][$fs][sub]}
	tmp=${filesystem[$host][$fs][tmp]}
	ARG+=" $fs $type $mount $sub $tmp"
	printlog "Starting scan of $host:$fs" 
	#
	# Update scan script on target host, initiate duc and refresh web page when done.
	#
	$SSH -A $SSHARG -n $GATEWAY "$COPY $SSH $SSHARG $host ./$SCAN $ARG && $CURL -s -o /dev/null $WEBURL" &
	pid[$!]="$host:$fs"
	COPY=""		# Copying duc scan script only needed once per host
	sleep 1
    done
done


#
# Non-blocking process reaper.
# Log the completion of each scan.
#
retval=0
printlog "Waiting for scans to complete."
while [[ ${#pid[@]} -gt 0 ]]; do
    unset reaplist
    typeset -A reaplist
    for i in ${!pid[@]}; do
	if ! kill -0 $i 2> /dev/null; then
	    wait $i
	    return[i]=$?
	    retval=$(( retval | return[i] ))
	    printlog "Exit value for ${pid[i]} = ${return[i]}"
	    reaplist[${pid[i]}]=${return[i]}
	    unset "pid[$i]"
	fi
    done
    if [ ${#reaplist[@]} -gt 0 ]; then
	for host in "${!reaplist[@]}"; do
	    printlog "host $host finished"
	done
    else
	[[ ${#pid[@]} -gt 0 ]] && sleep 10
    fi
done

# Update timstamp of web server directory and rename the scan started timestamp
# file to indicate scan has been completed.
$SSH $SSHARG $WEBHOSTPUB "$TOUCH $WEBDIR/ducdb && $MV -f $WEBDIR/$LASTSCAN $WEBDIR/${LASTSCAN}.done"
$SSH $SSHARG -n $GATEWAY "$CURL -s -o /dev/null $WEBURL"

printlog "Master driver script $0 finished with exit value $retval"
rm $LOCK
exit $retval
