#!/bin/bash
#
# Program to create duc database files for entire filesystems or top-level user directories.
# and copy the resulting database files to a web server to be viewed at,
# http://ldas-gridmon.ligo.caltech.edu/diskusage
#
# Keep top-level trend data on each user (size/apparent/files)
#   - Text file and archive sortable html tables (without hyperlinks to ducdb files)
#   - Some basic plotting
#
# Consider how to handle non-zero exit status from "duc index" and report back up the
# chain all the way to cron sending email, and don't replace ducdb file on web server
#
# Add optimization to not scan filesystems that haven't changed
# ZFS: use written@snapshot
# VSM: check last samfsdump
#
# Move to git.ligo.org project
#
# Copy duc files to shell accessible location as well as web server
#
# Stuart Anderson (Feb 16, 2015)
#


RM=/usr/bin/rm
CHMOD=/usr/bin/chmod
MKDIR=/usr/bin/mkdir
RSYNC=/usr/bin/rsync
DUCDIR=duc
DUCDBDIR=ducdb
LOGDIR=log
STATDIR=stat
PARALLEL=2
PID=$$
SLEEP=1
DUC=/usr/bin/duc
DUCEXCLUDE="--exclude .migrated_to_zfs --exclude .migrated_to_vsm --exclude .inodes --exclude .archive --exclude .stage --exclude .domain --exclude .fuid"
if [ ! -x $DUC ]; then
    DUC=/opt/csw/bin/amd64/duc
    if [ ! -x $DUC ]; then
	echo "Unable to find duc executable"
	exit 1
    fi
fi


#
# Parse command line arguments
#
if [ ! $# -ge 8 -o $(($#%5)) -ne 3 ]; then
    echo "Usage: $0 WebHost WebDir Host Name Type Mount SubDir Tmp [Name Type Mount SubDir Tmp] ..."
    echo ""
    echo "WebHost = remote host to store scan results"
    echo ""
    echo "WebDir = directory on remote host to store scan results"
    echo ""
    echo "Host = hostname of system being scanned with duc"
    echo ""
    echo "Name = filesytem name to scan with duc"
    echo ""
    echo "Type = {filesystem, user, inactive}"
    echo "  filesystem - single scan of the total filesystem specificed"
    echo "  user - in addition scan each user directory"
    echo "  inactive - in addition scan each inactive user directory"
    echo ""
    echo "Mount = mount point to be indexed by duc"
    echo ""
    echo "SubDir = location to additionaly scan individual directories if type is not Filesystem"
    echo ""
    echo "Tmp = temporary local location to write duc output before transferring to WebHost"
    echo ""
    exit 1;
fi

RHOST=$1
shift
RDIR=$1
shift
HOSTNAME=$1
shift
filesystem=()
typeset -A param=()
typeset -A return=()
typeset -A pid=()
while [[ $# > 0 ]]; do
    fs=$1
    filesystem+=("$fs")
    shift
    param["$fs:type"]=$1
    shift
    param["$fs:mount"]=$1
    shift
    param["$fs:subdir"]=$1
    shift
    param["$fs:tmp"]=$1
    shift

    #
    # Cleanup and create new local cache directories
    #
    BASE=${param[$fs:tmp]}/$DUCDIR
    $MKDIR -p $BASE
    $CHMOD 0700 $BASE
    for DIR in $DUCDBDIR $LOGDIR $STATDIR; do
	$MKDIR -p $BASE/$DIR/${param[$fs:type]}
    done
done


if [ "$(hostname)" != "$HOSTNAME" ]; then
    echo "Warning: $(hostname) != $HOSTNAME"
#    exit 2;
fi


#
# Function to print log messages
#
printlog () {
    echo "$(date) $HOSTNAME $@"
}


#
# Get the exit valie for pid
#
getexit() {
    if [ $# -ne 1 ]; then
	printlog "Internal error: getexit() called with $# rather than 1 argument"
	exit 1
    fi

    wait $1
    retval=$?
    if [ $retval -ne 0 ]; then
	 printlog "Exit value for ${pid[$1]} = $retval"
    fi
    return[$1]=$(( return[$1] | $retval ))
    unset "pid[$1]"
}

#
# Reap the exit values from any background processes that have finished
# and logically OR their values into the hash of return value pid's.
#
reapdone() {
    for i in ${!pid[@]}
    do
	if ! kill -0 "$i" 2>/dev/null; then
	    getexit $i
        fi
    done
}

#
# Block and reap the exit values from all background processes
# and logically OR their values into the hash of return value pid's.
#
reapall() {
    for i in ${!pid[@]}
    do
        getexit $i
    done
}


#
# Function to run duc and transfer results back to web server
# runduc Name Mount Type
#
runduc() {
    typeset Name=$1
    typeset Mount=$2
    typeset Type=$3
    typeset RemoteDatabase=$RDIR/ducdb/$Type
    typeset RemoteLog=$RDIR/log/$Type
    typeset RemoteStat=$RDIR/stat/$Type
    typeset DIR=${param[$Name:tmp]}/$DUCDIR
    typeset STAT=$DIR/$STATDIR/$Type/$HOSTNAME.$(date +"%Y%m%d").stat
    typeset LOG=$DIR/$LOGDIR/$Type/$HOSTNAME.$Name.log
    typeset DUCDB=$DIR/$DUCDBDIR/$Type/$Name.duc

    #
    # Wait for an empty process slot and collect any completed exit values
    #
    while [ `pgrep -P $PID | wc -l` -gt $PARALLEL ]; do
        sleep $SLEEP
    done
    reapdone

    #
    # Temporary hack to exclude vsmarchive:/cold?/migrate directories (Aug 2020)
    #
    [ "$HOSTNAME" == "vsmarchive" ] && DUCEXCLUDE+=" --exclude migrate"

    ($RM -f $DUCDB && $DUC index -v -d $DUCDB $DUCEXCLUDE $Mount > $LOG 2>&1 && $DUC info -a -b -d $DUCDB | tail -1 >> $STAT && $RSYNC -a $DUCDB $RHOST:$RemoteDatabase && $RM $DUCDB && $RSYNC -a $LOG $RHOST:$RemoteLog && $RSYNC -a $STAT $RHOST:$RemoteStat ) &
    pid[$!]="${HOSTNAME}:${DUCDB}"
#   printlog "Started $Name as pid $!"
}


#
# Loop over filesytems and start scans
#
printlog "Started $0"
for fsname in "${filesystem[@]}"; do
    MOUNT=${param[$fsname:mount]}
    runduc $fsname $MOUNT filesystem
done

#
# Loop over user accounts and start scans
#
for fsname in "${filesystem[@]}"; do
    MOUNT=${param[$fsname:mount]}
    if [ "${param[$fsname:type]}" != "filesystem" ]; then
	for dir in $MOUNT/${param[$fsname:subdir]}/*; do
	    case "$dir" in
	    */DELETE*)
		# Ignore
	    ;;
	    */lost+found)
		# Ignore
	    ;;
	    */tmp)
		# Ignore
	    ;;
	    *)
		runduc $(basename $dir) $dir ${param[$fsname:type]}
		sleep $SLEEP
	    ;;
	    esac
	done
    fi
done

#
# Block and gather all exit values
#
reapall


#
# Logical OR all the return values from background processes and exit with that value.
#
retval=0
for i in ${!return[@]}
do
    retval=$(( retval | ${return[$i]} ))
done


printlog "$0 exit $retval."
exit $retval
